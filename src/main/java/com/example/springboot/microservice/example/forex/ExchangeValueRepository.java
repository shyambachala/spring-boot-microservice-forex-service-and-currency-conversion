package com.example.springboot.microservice.example.forex;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ExchangeValueRepository extends JpaRepository<ExchangeValue, Long> {

//	ExchangeVaule is the entity which is managed in the database
//	Long is the primary key for the entity in the database
	ExchangeValue findByFromAndTo(String from, String to);

}
